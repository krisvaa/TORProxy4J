package connection.relay;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;

public class RelayDirectory {

    public static RelayDirectory instance;

    private final ArrayList<TorRelay> RELAYS = new ArrayList<>(Arrays.asList(
            new TorRelay("85.195.235.248", 9001, 9030, "103336165A0D2EFCAD3605339843A0A7710B8B92", true, false),
            new TorRelay("185.184.192.252", 9001, 9030, "01F73DBD9B56C31E3D3AAEB95F8CF1DEB7D9A72F", true, false),
            new TorRelay("185.72.244.37", 443, 80,"ECFDEA48C8A9F45A7241AEDA3C386F4D82F89689", true, false),
            new TorRelay("62.210.137.212", 443, 80,"F813C2EC410101E363AAF0C08D8C4B14EA614564", true, false),
            //new TorRelay("138.201.169.12", 443, 80,"DBE82FA23B9FE3CB2462A6FCF5289DED3CBF4AEE", true, false),
            new TorRelay("5.2.69.50", 9001, -1, "0B1120660999AD1022D08664BE1AD08A77F55E50", false, true),
            new TorRelay("144.172.73.50", 443, 80, "D42481A79771ADBB81B7DB5D2538815DCBE7B162", false, true),
            new TorRelay("185.243.218.27", 8443, -1, "5B9086D4BFB9EA36C95897DAED72FC3973847B43", false, true),
            new TorRelay("188.68.58.0", 9001, -1, "C2CD35F0766CAE4184F75299186FE8CF1A131E61", false, true),
            new TorRelay("185.220.100.247", 9000, -1, "5E52BEA22130598F200D05C42BB66709C24E8F67", false, true),
            new TorRelay("89.58.27.84", 1443, -1, "82D1A2DBABE623D420122B1D59965277007B9446", false, true),
            new TorRelay("46.38.247.22", 465, -1, "56784608242CB15B70ED6CBB8F40EEA3B62AF69E", false, true),
            new TorRelay("51.68.204.139", 9001, -1, "9AB93B5422149E5DFF4BE6A3814E2F6D9648DB6A", true, false),
            new TorRelay("103.251.167.10", 443, -1, "AF8DB275960279B87F098B16CC9C78092E118DB3", true, true)
    ));

    private final ArrayList<TorRelay> DIRECTORY_PICK_LIST;
    private final ArrayList<TorRelay> RELAY_PICK_LIST = new ArrayList<>(RELAYS);

    private RelayDirectory() {
        DIRECTORY_PICK_LIST = buildDirectoryList();
    }

    public static RelayDirectory getInstance() {
        if(instance == null) instance = new RelayDirectory();
        return instance;
    }

    private ArrayList<TorRelay> buildDirectoryList() {
        return RELAYS.stream().filter(TorRelay::isDirectoryNode).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Picks a relay and removes it from the RELAY PICK LIST. Two relays cannot occur twice on the same instance.
     *
     * @return Random relay removed from the pick_list.
     */
    public TorRelay pickRandomRelay(RelayType type) {
        Random r = new Random();
        ArrayList<TorRelay> pickList = null;

        switch (type) {
            case EXIT_NODE:
                pickList = RELAY_PICK_LIST.stream().filter(TorRelay::supportsExit).collect(Collectors.toCollection(ArrayList::new));
                break;
            case ENTRY_NODE:
                pickList = RELAY_PICK_LIST.stream().filter(TorRelay::isSuitableEntryNode).collect(Collectors.toCollection(ArrayList::new));
                break;
            case MIDDLE_NODE:
                pickList = RELAY_PICK_LIST.stream().filter(t -> !t.supportsExit()).collect(Collectors.toCollection(ArrayList::new));
                break;
        }

        TorRelay rl = pickList.get(r.nextInt(pickList.size()));
        RELAY_PICK_LIST.remove(rl);
        return rl;
    }

    /**
     * Returns a random relay from the global relay list. The item will not be deleted form the list after being picked.
     *
     * @return Random relay from the global relay list.
     */
    public TorRelay getRandomRelay() {
        Random r = new Random();
        return RELAYS.get(r.nextInt(RELAYS.size()-1));
    }

    /**
     * Picks a relay and removes it from the DIRECOTRY PICK LIST. Two relays cannot occur twice on this method on the same instance.
     *
     * @return Random relay, removed from the directory pick list.
     */
    public TorRelay pickRandomDirectoryRelay() {
        Random r = new Random();
        return DIRECTORY_PICK_LIST.remove(r.nextInt(DIRECTORY_PICK_LIST.size()-1));
    }

}
