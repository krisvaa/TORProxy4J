package connection;

import exceptions.TorException;

import javax.management.InstanceNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Implemented with skeleton-code from https://nick-lab.gs.washington.edu/java/jdk1.3.1/guide/rmi/sockettype.doc.html
 *
 */
public class TorSocket extends Socket {

    private final TorStream torStream;
    private final Circuit circuit;

    /**
     * Creates a new TorSocket with an TCP-Connection across a TorCircuit.
     *
     * Uses the shared Circuit from CircuitService-class. The shared Circuit must be loaded before initiating socket with this constructor.
     *
     * @param host Hostname or IPv4 address to connect to
     * @param port Port number to connect to
     * @param circuit Circuit to build the socket-connection on.
     * @throws IOException Any network related exceptions.
     */
    public TorSocket(String host, int port, Circuit circuit) throws IOException {
        super();
        try {
            this.circuit = circuit;
            torStream = circuit.createStream(new InetSocketAddress(host, port));
        } catch (TorException e) {
            e.printStackTrace();
            throw new IOException("Unable to create socket due to a Tor-related error.");
        }
    }

    /**
     * Creates a new TorSocket with an TCP-Connection across a TorCircuit.
     *
     * Uses the shared Circuit from CircuitService-class. The shared Circuit must be loaded before initiating socket with this constructor.
     *
     * @param host Hostname or IPv4 address to connect to
     * @param port Port number to connect to
     * @throws IOException Any network related exceptions.
     */
    public TorSocket(String host, int port) throws IOException, InstanceNotFoundException {
        super();
        try {
            this.circuit = CircuitService.getInstance().getCircuit();
            torStream = circuit.createStream(new InetSocketAddress(host, port));
        } catch (TorException e) {
            e.printStackTrace();
            throw new IOException("Unable to create socket due to a Tor-related error.");
        }
    }

    /**
     * Returns Input Stream of the related Tor-stream
     *
     * @return TorInputStream of the related Tor-Stream
     * @throws IOException If TorStream is not created/running
     */
    @Override
    public InputStream getInputStream() throws IOException {
        if(torStream == null || torStream.getInputStream() == null) {
            throw new IOException("Tor Stream not running.");
        }
        return torStream.getInputStream();
    }

    /**
     * Returns Input Stream of the related Tor-stream
     *
     * @return OutputStream of the related Tor-Stream
     * @throws IOException If TorStream is not created/running
     */
    @Override
    public OutputStream getOutputStream() throws IOException {
        if(torStream == null || torStream.getOutputStream() == null) {
            throw new IOException("Tor Stream not running.");
        }
        return torStream.getOutputStream();
    }


    /**
     * Closes the underlying connection
     *
     * @throws IOException Any exceptions related to networking.
     */
    public synchronized void close() throws IOException {
        OutputStream o = getOutputStream();
        o.flush();
        torStream.close();
        super.close();
    }

    public synchronized boolean isClosed() {
        return super.isClosed() || torStream.isClosed();
    }

    /**
     * Closes the underlying TorCircuit and ALL streams associated with it.
     *
     * @throws IOException Input Output error from closing circuit and streams.
     */
    public void closeCircuit() throws IOException {
        close();
        circuit.close();
    }

    /**
     * If the TCP-Connection of the underlying TorStream is active across the Circuit
     *
     * @return If the connection is active or not
     */
    public boolean isConnected() {
        return torStream.isConnected();
    }

    /**
     * Returns the Circuit which this Socket is active on.
     *
     * @return Circuit which this socket is active on
     */
    public Circuit getTorCircuit() {
        return circuit;
    }

    /**
     * Waits for the connection to be established. The function will halt until either the connection is sucsesful, or if the connection fails,
     *
     * @return Result of the connection, false if it failed, true of it succeeded.
     */
    public boolean waitForConnection() {
        return torStream.waitForConnection();
    }
}
