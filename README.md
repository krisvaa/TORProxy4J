[![pipeline status](https://gitlab.stud.idi.ntnu.no/krisvaa/TORProxy4J/badges/main/pipeline.svg)](https://gitlab.stud.idi.ntnu.no/krisvaa/TORProxy4J/-/commits/main)  [![Latest Release](https://gitlab.stud.idi.ntnu.no/krisvaa/TORProxy4J/-/badges/release.svg)](https://gitlab.stud.idi.ntnu.no/krisvaa/TORProxy4J/-/releases)

# 🧅 Tor4J

## Overview
Tor4J allows you to establish a secure communication model in your Java-application by using Onion Routing technology on the Open Tor-Network hosted by The Tor Project.


Fun fact: TOR = **T**he **O**nion **R**outer


- [Read more about TOR (Wikipedia)](https://en.wikipedia.org/wiki/Tor_(network))
- [Read more about the TorProject](https://www.torproject.org)

### Description
By connecting to the Tor-network all communication is untracable. In Tor4J you communicate trough a circuit consisting of 3 or more Tor-Relays. By utilizing multiple layers of encryption, Tor4J is able to send messages over the circuit without the middle- or end-nodes knowing the origin of the communication. The end-node will perform the communication on behalf of the client, without knowing it's origin. All other nodes will not know the client-origin, end-origin (TCP-Endpoint) or the data being sent. 

One message being sent is encrypted with N number of "Onion Skins", where N is the relay-count. For each relay-hop, one "Onion Skin" is peeled of (Decrypted), and the message is passed along the circuit. When arriving at the end, the message is decrypted, and the end-node can perform communication with the outer-world as requested. When the end-node sends a message back, it is encrypted with an "Onion Skin" for each relay-hop. The client has a decryption key for each layer, and will sucsesfully remove all Onion Skins and retrieve the message. Each "Onion Skin" is a AES-128 Bit encryption layer.

The TorProject Community hosts a large number of Tor-Relays, which is open for anyone to connect to.

Tor4J's protocol is written as specified in [tor-spec.txt](https://gitweb.torproject.org/torspec.git/tree/tor-spec.txt). 

### ⚙️ Tor4J Features
- Automatic Tor-Circuit Builder
- Establish Tor TCP-Sockets accross Tor-Circuit
- Uses the newer and more secure NTor handshake
- Available as Maven Dependency
- Automatic Unit-testing (CI)
- Automatic deployement to Maven Repository (CD)

### 🕗 Tor4J Upcomming Features
- Support for Tor-Directory lookup
- Tor TLS-Socket Support
- Support for Secret Server Communication (Onion Services / Deep Web)

### 🚧 Tor4J Planned Improvements 
- Improve code coverage of Unit-tests
- Improve Java-Documentation and implement Java-doc upload in CI/CD.
- Make [TorOutputStream](/src/main/java/connection/TorOutputStream.java) better conform to parent-type OutputStream such that it works with classes like PrintWriter.
- Make TorSocket better conform to parent-type Socket.
- Better conform to Tor-Protocol Specifications for Closing Circuits and/or Streams.


### 👎 Known weaknesess
- Hard-coded Directory Library
    - Requires recompile to remove/add servers
    - Will be replaced with Tor Directory Service implementation
- TorOutputStream does not work with PrintWriter
- No TLS-Support :(
- Very low code coverage (Unit testing)

## 📦 Dependencies
Tor4J require some dependencies to function correctly. Each dependency is listed below with a quick introduction, usage-space in Tor4J and it's corresponding maven-repository link.

### Bouncycastle v 1.7
[`org.bouncycaslte.bcprov-jdk15on`](https://mvnrepository.com/artifact/org.bouncycastle/bcprov-jdk15on)

Bouncycastle provides a massive Cryptography suite. Tor4J uses Bouncycastle for SHA256 Digests and Base64 Encoding and Decoding. It is also in use to generate key-result for the NTor-Handshake using BouncyCastle HKDF-Generator.

### Whispersystems Curve 25519 v 0.5.0
[`org.whispersystems.curve25519-java`](https://mvnrepository.com/artifact/org.whispersystems/curve25519-java)

Whispersystems Curve 25519 provides Curve 25519 Elliptic Curve Diffie-Hellman (Curve25519 ECDH) encryption which is the key agreement algorithm used in the NTor-Handshake.

### JUnit v 5.7
[`org.junit.jupiter`](https://mvnrepository.com/artifact/junit/junit)

JUnit provides a Unit-test suite for Java-code base. Tor4J uses it to run Unit-tests of it's source code. It is used to deliver a robust CI/CD system, which uses these tests to decide wether or not to perform automatic deployement. The dependency is not included in release-packages.

### GPG Plugin v 1.5
[`org.apache.maven.plugins.maven-gpg-plugin`](https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-gpg-plugin)

GPG Maven-Plugin allows Maven-jobs to take advantage of the GPG-Keytool for generating keys, and signing files. Tor4J uses the GPG tool to sign all files before uploading them to the Sonatype Maven-repository.

## 💾 Install
Tor4J is available as a Maven-Dependency. Since it is still in Beta, there are only SNAPSHOT-Versions available.

### Tor4J Requirements
Tor4J requires a Maven 4+ Project with Java 11 or higher.

First, add the SNAPSHOT-Repository:
```xml
<!-- Paste in Repositories-tag -->
<repository>
    <id>oss.sonatype.org-snapshot</id>
    <url>https://oss.sonatype.org/content/repositories/snapshots</url>
    <snapshots>
        <enabled>true</enabled>
    </snapshots>
</repository>
```

Then, add Tor4J to your Dependencies:
```xml
<!-- Paste in Dependencies-tag -->
<dependency>
    <groupId>no.kaars.tor4j</groupId>
    <artifactId>TOR4J</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```
# 📚 Usage
## Circuit

### Open Circuit
First step in using Tor4J is creating a Tor-Circuit. More often than not, one application can use the same single Circuit for all of it's communication.
CircuitService provides an endpoint to generate a single circuit to be used application-wide.

To generate a new circuit:
```java
Circuit circuit = CircuitService.getInstance().generateNewCircuit();
```

The CircuitService will now pick a default number of relays from the RelayDirectory. Then the CircuitService will call CircuitBuilder which will handle connection-establishment and NTor-Handshakes with all relays to exchange encryption- and digest-keys.


To generate a Circuit with `NUMBER_OF_RELAYS` amount of relays, the following method may be called:
```java
Circuit circuit = CircuitService.getInstance().generateNewCircuit(NUMBER_OF_RELAYS);
```
Note that the maximum relay-count is 8 due to constriants in the Tor-Protocol.

The above circuits can be retrieved application-wide by calling. Note that calling generateNewCircuit will close the existing circuit. 
```java
Circuit circuit = CircuitService.getInstance().getCircuit();
```

### Close Circuit
To close a Circuit, simply call:
```java
 circuit.close();
```

## Create TCP Socket (TorSocket)

### Open TorSocket
After sucsesfully creating a circuit, the client may create multiple TorStreams. A TorStream is a TCP-Stream trough the Tor-Circuit, the actual TCP-Connection is performed by the last relay in the circuit on behalf of the client. The TorSocket is an abstraction to allow the client to send and receive data trough the Tor-Circuit as if it were directly connected to the TCP-Destination. The TorSocket acctually inherit the Socket-class, which is the class used for regular TCP-Communication in Java.

Please note that TLS Sockets are not yet implemented, but can in theory be achived by using the existing TCP-Support.

Example on a TorSocket-creation to [datakom.no](http://datakom.no) on port 80:
```java
TorSocket socket = new TorSocket("datakom.no", 80);
s.waitForConnection();
```
To halt excecition until the connection is made one may call `s.waitForConnection()`. 

The example above use the existing Circuit from CircuitService, to explicitly specify circuit `CIRCUIT`, call:
```java
TorSocket socket = new TorSocket("datakom.no", 80, CIRCUIT);
```

One Circuit can hold multiple TorSockets thanks to the [`TorStreamRouter`](/src/main/java/connection/TorStreamRouter.java).

### Close TorSocket
To close a single TorSocket, call:
```java
 socket.close();
```

## Sending and Receiving data
Once a TorSocket is connected, data-flow works like a normal Socket by using InputStream and OutputStream

```java
BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream());
OutputStream output = socket.getOutputStream();

output.write("GET / HTTP/1.1\r\nHost:datakom.no\r\n\r\n");

while(true) {
    String response = reader.readLine();

    if(response == null) { break; }
    System.out.println(response);
}
```
The example above sends a standard HTTP-request to datakom.no, asking for it's index.html page. In the while-loop below, the client reads all the lines received by datakom.no trough the Tor-network.

# Example
Please refer to [TelnetExample.java](https://gitlab.stud.idi.ntnu.no/krisvaa/TORProxy4J/-/blob/main/src/main/java/TelnetExample.java) for an usage-example. The example provided shows an implementation of a TELNET-Client over TOR. Enter hostname as first argument, and portnumber as the second argument. After the connection is established, you may send and receive messages to and from the server.

### Closing Streams
To close a stream, the overlying TorSocket needs to be closed. See the section above on how this is accomplished. 

# 📝 Unit Tests
This project implement Unit Testing trough JUnit 5.

To run the tests on the project you may run the following command from the project root-catalog:
```unix
mvn test
```

# CI/CD
This project takes advantage of GitLab CI/CD. For each new commit to main-branch, the following stages are executed:

### Compile
Compiles the java-code so it is ready to be run for testing.

### Test
Runs the JUnit tests. The result is reported back in a Surefire-report to be read by GitLab.

### Deploy
This job compiles, creates hashes, signs files, and uploads these to sonatype.org Maven Repository.
